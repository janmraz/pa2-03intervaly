#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
class InvalidRangeException
{
};
#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRangeList;

class CRange
{
public:
    CRange(long long start,long long end);
    friend ostream &operator<< (ostream & os,const CRange & cRange);
    bool operator== (const CRange& cRange2) const;
    bool operator!= (const CRange& cRange2) const;
    CRangeList operator- (const CRange& cRange2);
    CRangeList operator+ (const CRange& cRange2);
    long long m_start;
    long long m_end;
private:
};

CRange::CRange(long long start, long long end): m_start(start), m_end(end) {
    if(m_start > m_end) throw InvalidRangeException();
}

ostream &operator<<(ostream &os, const CRange& cRange) {
    if(cRange.m_start == cRange.m_end) {
        os <<  cRange.m_start;
        return os;
    }
    os << "<" << cRange.m_start << ".." << cRange.m_end << ">";
    return os;
}

bool CRange::operator==(const CRange &cRange2) const {
    return m_start == cRange2.m_start && m_end == cRange2.m_end;
}

bool CRange::operator!=(const CRange &cRange2) const {
    return !(*this == cRange2);
}

class CRangeList
{
public:
    // constructor
    // Includes long long / range
    // += range / range list
    // -= range / range list
    // = range / range list
    // operator ==
    // operator !=
    // operator <<
    bool Includes(long long src) const;
    bool Includes(const CRange& cRange) const;
    void ensureConsistency(vector<CRange>::iterator &crIter);
    void udpateBoundary(vector<CRange>::iterator &crIter, const CRange& incoming);
    CRangeList();
    CRangeList(const CRangeList& src);

    const CRangeList & operator+= (const CRange& cRange);
    const CRangeList & operator+= (const CRangeList& cRangeList);
    const CRangeList & operator= (const CRange& cRange);
    const CRangeList & operator= (const CRangeList& cRangeList);
    const CRangeList & operator-= (const CRange& cRange);
    const CRangeList & operator-= (const CRangeList& cRangeList);
    friend ostream &operator<< (ostream & os,const CRangeList & cRangeList);
    bool operator== (const CRangeList& cRangeList2) const;
    bool operator!= (const CRangeList& cRangeList2) const;
    CRangeList &operator+ (const CRange &cRange2);
    CRangeList &operator- (const CRange &cRange2);
    int bigger_than (int src);
    int lesser_than (int src);

private:
    vector<CRange> m_list;
};


ostream& operator<< (ostream & os,const CRangeList & cRangeList) {
    bool isFirst = true;
    os << "{";
    for(const CRange& cRange : cRangeList.m_list){
        if(!isFirst)
            os << ",";
        isFirst = false;
        os << cRange;
    }
    os << "}";

    return os;
}

CRangeList CRange::operator+(const CRange &cRange2) {
    CRangeList cRangeList;
    cRangeList += *this;
    cRangeList += cRange2;
    return cRangeList;
}

CRangeList & CRangeList::operator+(const CRange &cRange2) {
    *this += cRange2;
    return *this;
}

CRangeList CRange::operator-(const CRange &cRange2) {
    CRangeList cRangeList;
    cRangeList += *this;
    cRangeList -= cRange2;
    return cRangeList;
}

CRangeList & CRangeList::operator-(const CRange &cRange2) {
    *this -= cRange2;
    return *this;
}

const CRangeList &CRangeList::operator=(const CRange &cRange) {
    m_list = vector<CRange>();
    m_list.push_back(cRange);
    return *this;
}

const CRangeList &CRangeList::operator=(const CRangeList &cRangeList) {
    m_list = cRangeList.m_list;
    return *this;
}

const CRangeList &CRangeList::operator+=(const CRangeList &cRangeList) {
    for(CRange cRange : cRangeList.m_list)
        *this += cRange;
    return *this;
}

const CRangeList &CRangeList::operator+=(const CRange &cRange) {
    vector<CRange>::iterator crIter;
    CRange cr = cRange;
    for(crIter=m_list.begin();crIter!=m_list.end();++crIter){
        long long st = crIter->m_start == LLONG_MIN ? crIter->m_start : crIter->m_start - 1;
        long long en = crIter->m_end == LLONG_MAX ? crIter->m_end : crIter->m_end + 1;
        if(cr.m_end < st){
            // | | St En - insert new interval
            m_list.insert(crIter,cRange);
            return *this;
        } else if (cr.m_start <= st && cr.m_end >= st && cr.m_end <= en) {
            // | St | En - set new start
            crIter->m_start = cr.m_start;
            ensureConsistency(crIter);
            return *this;
        } else if (cr.m_start >= st && cr.m_start <= en && cr.m_end >= en) {
            // St | En | - set new end
            crIter->m_end = cr.m_end;
            ensureConsistency(crIter);
            return *this;
        } else if (cr.m_start <= st && cr.m_end >= en) {
            // | St En | - enlarge end and start
            crIter->m_end = cr.m_end;
            crIter->m_start = cr.m_start;
            ensureConsistency(crIter);
            return *this;
        } else if (cr.m_start >= st && cr.m_end <= en) {
            // St | | En - ignore
            return *this;
        }
    }
    // St En | |
    m_list.push_back(cRange);
    return *this;
}

int CRangeList::lesser_than(int src) {
    size_t mid, left = 0 ;
    size_t right = m_list.size(); // one position passed the right end
    while (left < right) {
        mid = left + (right - left)/2;
        if (src > m_list[mid].m_end){
            left = mid+1; // go right
        }
        else if (src < m_list[mid].m_end){
            right = mid; // go left
        }
        else {
            return mid;
        }
    }

    return -1;
}

int CRangeList::bigger_than(int src) {
    size_t mid, left = 0;
    size_t right = m_list.size(); // one position passed the right end
    while (left < right) {
        mid = left + (right - left)/2;
        if (src > m_list[mid].m_start){
            left = mid+1; // go right
        }
        else if (src < m_list[mid].m_start){
            right = mid; // go left
        }
        else {
            return mid;
        }
    }

    return -1;
}

const CRangeList &CRangeList::operator-=(const CRange &cRange) {
    auto bottom = upper_bound (m_list.begin(), m_list.end(), cRange.m_start,[](long long value, const CRange & a){
        return value < a.m_start;
    });
    auto top = upper_bound (m_list.begin(), m_list.end(), cRange.m_end,[](long long value, const CRange & a){
        return value < a.m_end;
    });
    if(top == bottom){
        udpateBoundary(bottom,cRange);
        return *this;
    }
    udpateBoundary(bottom,cRange);
    udpateBoundary(top,cRange);
    ++bottom;
    if(bottom != top){
        m_list.erase(bottom, top);
    }
    return *this;
}

const CRangeList &CRangeList::operator-=(const CRangeList &cRangeList) {
    for(CRange cRange : cRangeList.m_list)
        *this -= cRange;
    return *this;
}

bool CRangeList::operator!=(const CRangeList &cRangeList2) const {
    return !(*this == cRangeList2);
}

bool CRangeList::operator==(const CRangeList &cRangeList2) const {
    auto crIter1 = m_list.begin();
    auto crIter2 = cRangeList2.m_list.begin();
    for(;crIter1 != m_list.end() && crIter2 != cRangeList2.m_list.end();) {
        if(*crIter1 != *crIter2)
            return false;
        ++crIter1;
        ++crIter2;
    }

    return true;
}

CRangeList::CRangeList() {

}

bool CRangeList::Includes(long long src) const {
    size_t mid, left = 0 ;
    size_t right = m_list.size(); // one position passed the right end
    while (left < right) {
        mid = left + (right - left)/2;
        if (src > m_list[mid].m_end){
            left = mid+1;
        }
        else if (src < m_list[mid].m_start){
            right = mid;
        }
        else {
            return true;
        }
    }

    return false;
}

bool CRangeList::Includes(const CRange &cRange) const {
    for(auto crIter=m_list.begin();crIter!=m_list.end();++crIter)
        if(cRange.m_start >= crIter->m_start && cRange.m_end <= crIter->m_end)
            return true;
    return false;
}

CRangeList::CRangeList(const CRangeList &src) {
    m_list = src.m_list;
}

void CRangeList::ensureConsistency(vector<CRange>::iterator &crIter) {
    CRange cr = *crIter;
    crIter++;
    for(;crIter!=m_list.end();){
        long long st = crIter->m_start == LLONG_MIN ? crIter->m_start : crIter->m_start - 1;
        long long en = crIter->m_end == LLONG_MAX ? crIter->m_end : crIter->m_end + 1;
        if(cr.m_end < st){
            // | | St En - insert new interval
            return;
        } else if (cr.m_start <= st && cr.m_end >= st && cr.m_end <= en) {
            // | St | En - set new start
            crIter->m_start = cr.m_start;
            m_list.erase(--crIter);
        } else if (cr.m_start >= st && cr.m_start <= en && cr.m_end >= en) {
            // St | En | - set new end
            crIter->m_end = cr.m_end;
            m_list.erase(--crIter);
        } else if (cr.m_start <= st && cr.m_end >= en) {
            // | St En | - enlarge end and start
            m_list.erase(crIter);
        } else if (cr.m_start >= st && cr.m_end <= en) {
            // St | | En - ignore
            return;
        }
    }
    // St En | | - ignore
}

void CRangeList::udpateBoundary(vector<CRange>::iterator &crIter, const CRange& incoming) {
    long long st = crIter->m_start == LLONG_MIN ? crIter->m_start : crIter->m_start - 1;
    long long en = crIter->m_end == LLONG_MAX ? crIter->m_end : crIter->m_end + 1;
    if(incoming.m_end < st){
        // | | St En - if interval is full before so ignore it
        return;
    } else if (incoming.m_start == crIter->m_start && incoming.m_end == crIter->m_end) {
        // exact match
        m_list.erase(crIter);
        if(m_list.size() == 0) return;
    } else if (incoming.m_start <= st && incoming.m_end >= st && incoming.m_end <= en) {
        // | St | En - overlaps from beginning
        if(incoming.m_end == crIter->m_end){
            m_list.erase(crIter);
            return;
        }
        crIter->m_start = incoming.m_end + 1;
    } else if (incoming.m_start > st && incoming.m_start <= en && incoming.m_end >= en) {
        // St | En | -  overlaps from end
        if(incoming.m_start == crIter->m_start){
            m_list.erase(crIter);
            return;
        }
        crIter->m_end = incoming.m_start - 1;
    } else if (incoming.m_start <= st && incoming.m_end >= en) {
        // | St En | - overlaps whole interval
        m_list.erase(crIter);
    }else if (incoming.m_start >= st && incoming.m_end <= en) {
        // St | | En - interval is inside so divide it into two parts
        if(incoming.m_start == crIter->m_start){
            crIter->m_start = incoming.m_end + 1;
            return;
        }

        long long previousEnd = crIter->m_end;
        crIter->m_end = incoming.m_start - 1;

        if(incoming.m_end != previousEnd)
            m_list.insert(++crIter,CRange(incoming.m_end + 1,previousEnd));
    }
}


#ifndef __PROGTEST__
string             toString                                ( const CRangeList& x )
{
    ostringstream oss;
    oss << x;
    return oss . str ();
}

void test0b() {

    CRangeList a;
    a = CRange(10, 10) + CRange(20, 20) + CRange(12, 12) + CRange(18, 18);
    assert ( toString ( a ) == "{10,12,18,20}" );
    a -= CRange(11, 19);
    assert ( toString ( a ) == "{10,20}" );
    a = CRange(10, 10) + CRange(20, 20) + CRange(12, 12) + CRange(18, 18);
    assert ( toString ( a ) == "{10,12,18,20}" );
    a -= CRange(10, 20);
    assert ( toString ( a ) == "{}" );
    a = CRange(10, 100);
    a -= CRange(20, 80);
    assert(toString(a) == "{<10..19>,<81..100>}");
    a = CRange(10, 100);
    a -= CRange(11, 99);
    assert(toString(a) == "{10,100}");
    a = CRange(10, 100);
    a -= CRange(11, 101);
    assert(toString(a) == "{10}");
    a = CRange(10, 100);
    a -= CRange(50, 150);
    assert(toString(a) == "{<10..49>}");
    a = CRange(10, 100);
    a -= CRange(0, 50);
    assert(toString(a) == "{<51..100>}");
    a = CRange(10, 100);
    a -= CRange(0, 99);
    assert(toString(a) == "{100}");
    a = CRange(10, 100);
    a -= CRange(10, 100);
    assert(toString(a) == "{}");
    a = CRange(10, 100);
    a -= CRange(0, 80);
    assert(toString(a) == "{<81..100>}");
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a -= CRange(11, 29);
    assert(toString(a) == "{<0..10>,<30..40>}");
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a -= CRange(10, 30);
    assert(toString(a) == "{<0..9>,<31..40>}");
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(0, 40);
    assert ( toString ( a ) == "{<-10..-5>,<50..60>}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(1, 39);
    assert ( toString ( a ) == "{<-10..-5>,0,40,<50..60>}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(2, 38);
    assert ( toString ( a ) == "{<-10..-5>,<0..1>,<39..40>,<50..60>}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(-5, 50);
    assert ( toString ( a ) == "{<-10..-6>,<51..60>}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(-8, 55);
    assert ( toString ( a ) == "{<-10..-9>,<56..60>}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(-9, 59);
    assert ( toString ( a ) == "{-10,60}" );
    a = CRange(0, 10);
    a += CRange(12, 28);
    a += CRange(30, 40);
    a += CRange(-10, -5);
    a += CRange(50, 60);
    a -= CRange(-10, 60);
    assert ( toString ( a ) == "{}" );
    a = CRange(2, 2);
    a += CRange(4, 4);
    a -= CRange(2, 2);
    a -= CRange(4, 4);
    assert ( toString ( a ) == "{}" );
    a = CRange(2, 2);
    a += CRange(4, 4);
    a += CRange(0, 0);
    a += CRange(6, 6);
    a -= CRange(2, 2);
    a -= CRange(4, 4);
    assert ( toString ( a ) == "{0,6}" );
    a = CRange(0, 0);
    a += CRange(10, 10);
    a += CRange(2, 8);
    a -= CRange(2, 2);
    a -= CRange(8, 8);
    assert ( toString ( a ) == "{0,<3..7>,10}" );
    a = CRange(0, 0);
    a += CRange(10, 10);
    a += CRange(5, 5);
    a -= CRange(5, 5);
    assert ( toString ( a ) == "{0,10}" );
    a = CRange(0, 10);
    a -= CRange(0, 0);
    a -= CRange(10, 10);
    assert ( toString ( a ) == "{<1..9>}" );
    a = CRange(0, 10);
    a += CRange(12, 20);
    a += CRange(22, 30);
    a -= CRange(11, 11);
    a -= CRange(21, 21);
    assert ( toString ( a ) == "{<0..10>,<12..20>,<22..30>}" );
    a = CRange(0, 10);
    a += CRange(12, 20);
    a += CRange(22, 30);
    a -= CRange(5, 5);
    a -= CRange(25, 25);
    assert ( toString ( a ) == "{<0..4>,<6..10>,<12..20>,<22..24>,<26..30>}" );
    a = CRange(0, 10);
    a += CRange(12, 20);
    a += CRange(22, 30);
    a -= CRange(-1, -1);
    a -= CRange(31, 31);
    assert ( toString ( a ) == "{<0..10>,<12..20>,<22..30>}" );
    a -= a;
    for (int sf = 0; sf <= 60; sf++) {
        if (sf % 2 == 0) {
            a -= CRange(sf, sf);
        } else {
            a += CRange(sf, sf);
        }
    }
    assert ( toString ( a ) == "{1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59}" );
    a -= CRange(2, 58);
    assert ( toString ( a ) == "{1,59}" );
    a = CRange(10, 50);
    a += CRange(0, 8);
    a += CRange(52, 60);
    a += CRange(9, 51);
    assert ( toString ( a ) == "{<0..60>}" );
}

void basicTests(){
    CRangeList a, b;
    assert ( sizeof ( CRange ) <= 2 * sizeof ( long long ) );
    a = CRange ( 5, 10 );
    a += CRange ( 25, 100 );
    assert ( toString ( a ) == "{<5..10>,<25..100>}" );
    a += CRange ( -5, 0 );
    a += CRange ( 8, 50 );
    assert ( toString ( a ) == "{<-5..0>,<5..100>}" );
    a += CRange ( 101, 105 ) + CRange ( 120, 150 ) + CRange ( 160, 180 ) + CRange ( 190, 210 );
    assert ( toString ( a ) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}" );
    a += CRange ( 106, 119 ) + CRange ( 152, 158 );
    assert ( toString ( a ) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}" );
    a += CRange ( -3, 170 );
    a += CRange ( -30, 1000 );
    assert ( toString ( a ) == "{<-30..1000>}" );
    b = CRange ( -500, -300 ) + CRange ( 2000, 3000 ) + CRange ( 700, 1001 );
    a += b;
    assert ( toString ( a ) == "{<-500..-300>,<-30..1001>,<2000..3000>}" );
    a -= CRange ( -400, -400 );
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}" );
    a -= CRange ( 10, 20 ) + CRange ( 900, 2500 ) + CRange ( 30, 40 ) + CRange ( 10000, 20000 );
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    try
    {
        a += CRange ( 15, 18 ) + CRange ( 10, 0 ) + CRange ( 35, 38 );
        assert ( "Exception not thrown" == NULL );
    }
    catch ( const InvalidRangeException & e )
    {
    }
    catch ( ... )
    {
        assert ( "Invalid exception thrown" == NULL );
    }
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    b = a;
    assert ( a == b );
    assert ( !( a != b ) );
    b += CRange ( 2600, 2700 );
    assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    assert ( a == b );
    assert ( !( a != b ) );
    b += CRange ( 15, 15 );
    assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}" );
    assert ( !( a == b ) );
    assert ( a != b );
    assert ( b . Includes ( 15 ) );
    assert ( b . Includes ( 2900 ) );
    assert ( !b . Includes ( -213412351235 ) );
    assert ( b . Includes ( CRange ( 15, 15 ) ) );
    assert ( b . Includes ( CRange ( -350, -350 ) ) );
    assert ( b . Includes ( CRange ( 100, 200 ) ) );
    assert ( !b . Includes ( CRange ( 800, 900 ) ) );
    assert ( !b . Includes ( CRange ( -1000, -450 ) ) );
    assert ( !b . Includes ( CRange ( 0, 500 ) ) );
    a += CRange ( -10000, 10000 ) + CRange ( 10000000, 1000000000 );
    assert ( toString ( a ) == "{<-10000..10000>,<10000000..1000000000>}" );
    b += a;
    assert ( toString ( b ) == "{<-10000..10000>,<10000000..1000000000>}" );
    b -= a;
    assert ( toString ( b ) == "{}" );
    b += CRange ( 0, 100 ) + CRange ( 200, 300 ) - CRange ( 150, 250 ) + CRange ( 160, 180 ) - CRange ( 170, 170 );
    assert ( toString ( b ) == "{<0..100>,<160..169>,<171..180>,<251..300>}" );
    b -= CRange ( 10, 90 ) - CRange ( 20, 30 ) - CRange ( 40, 50 ) - CRange ( 60, 90 ) + CRange ( 70, 80 );
    assert ( toString ( b ) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}" );
    b = CRange(10,90) - CRange(10,20);
    assert ( toString ( b ) == "{<21..90>}" );
    b -= CRange(89,90);
    assert ( toString ( b ) == "{<21..88>}" );
    b -= CRange(21,88);
    assert ( toString ( b ) == "{}" );
    b += CRange ( 0, 100 ) + CRange ( 200, 300 ) - CRange ( 150, 250 ) + CRange ( 160, 180 ) - CRange ( 170, 170 );
    assert ( toString ( b ) == "{<0..100>,<160..169>,<171..180>,<251..300>}" );
    b -= CRange(50,180);
    assert ( toString ( b ) == "{<0..49>,<251..300>}" );
    b = CRange ( 0, 100 ) + CRange ( 200, 300 ) - CRange ( 150, 250 ) + CRange ( 160, 180 ) - CRange ( 170, 170 );
    assert ( toString ( b ) == "{<0..100>,<160..169>,<171..180>,<251..300>}" );
    b -= CRange(50,171);
    assert ( toString ( b ) == "{<0..49>,<172..180>,<251..300>}" );
    b = CRange ( 0, 100 ) + CRange ( 200, 300 );
    b -= CRange(0,70);
    assert ( toString ( b ) == "{<71..100>,<200..300>}" );
    b = CRange ( 0, 100 ) + CRange ( 200, 300 );
    b -= CRange(0,121);
    assert ( toString ( b ) == "{<200..300>}" );
    b = CRange ( 0, 100 ) + CRange ( 200, 300 );
    b -= CRange(-5,0);
    assert ( toString ( b ) == "{<1..100>,<200..300>}" );
    b = CRange ( 0, 100 ) + CRange ( 200, 300 );
    b -= CRange(100,200);
    assert ( toString ( b ) == "{<0..99>,<201..300>}" );
#ifdef EXTENDED_SYNTAX
    CRangeList x { { 5, 20 }, { 150, 200 }, { -9, 12 }, { 48, 93 } };
  assert ( toString ( x ) == "{<-9..20>,<48..93>,<150..200>}" );
  ostringstream oss;
  oss << setfill ( '=' ) << hex << left;
  for ( const auto & v : x + CRange ( -100, -100 ) )
    oss << v << endl;
  oss << setw ( 10 ) << 1024;
  assert ( oss . str () == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======" );
#endif /* EXTENDED_SYNTAX */
}

int                main                                    ( void )
{
    basicTests();
    test0b();
    return 0;
}
#endif /* __PROGTEST__ */
